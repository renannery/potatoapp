package com.origamitecnologia.hotpotato.interfaces;

        import android.location.Location;

public interface ILocationTracker {
    public void onConnected();
    public void onLocationChanged(Location location);

}
