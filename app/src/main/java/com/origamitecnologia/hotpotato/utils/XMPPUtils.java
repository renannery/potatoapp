package com.origamitecnologia.hotpotato.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.origamitecnologia.hotpotato.managers.GenericBus;
import com.origamitecnologia.hotpotato.models.ChatMessage;
import com.origamitecnologia.hotpotato.models.User;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.address.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.attention.packet.AttentionExtension;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.commands.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.delay.provider.DelayInformationProvider;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.disco.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.iqlast.packet.LastActivity;
import org.jivesoftware.smackx.iqprivate.PrivateDataManager;
import org.jivesoftware.smackx.muc.packet.GroupChatInvitation;
import org.jivesoftware.smackx.offline.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.offline.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.privacy.provider.PrivacyProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.search.UserSearch;
import org.jivesoftware.smackx.sharedgroups.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.si.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.vcardtemp.provider.VCardProvider;
import org.jivesoftware.smackx.xdata.provider.DataFormProvider;

import java.io.File;
import java.io.IOException;

import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class XMPPUtils implements ChatMessageListener, ChatManagerListener, ConnectionListener, FileTransferListener {
    public static XMPPUtils instance;

    private AbstractXMPPConnection connection;
    private static Context context;
    private static String TAG = "XMPP FILE";
    private static String BASEDIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static User user;
    private static String currentUsername;

    public static XMPPUtils getInstance(Context ctx) {
        if(instance == null) {
            instance = new XMPPUtils();
            context = ctx;
        }
        return instance;
    }

    public static XMPPUtils getInstance() {
        if(instance == null) {
            instance = new XMPPUtils();
        }
        return instance;
    }

    public void  connect() {
        user = UserUtils.getInstance().getLoggedUser(context);
        currentUsername = user.getChatUsername();
        //TODO add registered user
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        configBuilder.setUsernameAndPassword(currentUsername, "Senha2015");
//        configBuilder.setUsernameAndPassword("renannery", "Cebola20");
        configBuilder.setServiceName(Config.XMPP_HOST);
        configBuilder.setSendPresence(true);
        configBuilder.setConnectTimeout(10000);
        configureProvider();
        connection = new XMPPTCPConnection(configBuilder.build());
        ServiceDiscoveryManager.getInstanceFor(connection).addFeature("http://jabber.org/protocol/disco#info");
        ServiceDiscoveryManager.getInstanceFor(connection).addFeature("jabber:iq:privac y");
        ServiceDiscoveryManager.getInstanceFor(connection).addFeature("http://jabber.org/protocol/si");

        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void login() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    connection.connect();
                    connection.login();
                    FileTransferManager.getInstanceFor(connection).addFileTransferListener(XMPPUtils.this);
                    ChatStateManager.getInstance(connection);
                    ChatManager.getInstanceFor(connection).addChatListener(XMPPUtils.this);
                    ReconnectionManager.getInstanceFor(connection).enableAutomaticReconnection();
                    connection.addConnectionListener(XMPPUtils.this);

                } catch (SmackException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XMPPException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void logout() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Presence presence = new Presence(org.jivesoftware.smack.packet.Presence.Type.unavailable);
                presence.setStatus(connection.getUser());

                try {
                    connection.disconnect(presence);
                    EventBus.getDefault().unregister(this);
                } catch (Exception exception) {
                    exception.printStackTrace();
                    connection.disconnect();
                }
            }
        }).start();
    }

    public void onEventMainThread(GenericBus event) {
        if(event.getKey() == GenericBus.CONNECTION_STATE) {
        }
    }

    public AbstractXMPPConnection getConnection() {
        return connection;
    }

    public void sendMessage(Message message) throws XMPPException {

        Chat chat = ChatManager.getInstanceFor(XMPPUtils.getInstance().getConnection()).createChat(message.getTo().concat("@").concat(Config.XMPP_HOST), this);
        ChatMessage chatMessage = new ChatMessage(message);
        chatMessage.setIsDisplay(true);

        try {
            chat.sendMessage(message);
            chatMessage.setIsSent(true);
        } catch (SmackException e) {
            e.printStackTrace();
            chatMessage.setIsSent(false);
        } finally {
            Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            ChatMessage realmChatMessage = realm.copyToRealmOrUpdate(chatMessage);
            realm.commitTransaction();
            realm.close();
        }
    }

    public void sendFile(String to) {
        OutgoingFileTransfer outgoingFileTransfer = FileTransferManager.getInstanceFor(connection).createOutgoingFileTransfer(to);

        final File file = new File(BASEDIR + "/Download/icon_test.png");

        try {
            outgoingFileTransfer.sendFile(file, "The current jar file");

            while (!outgoingFileTransfer.isDone()) {
                Log.d(TAG, "File transfer status: " + outgoingFileTransfer.getStatus());
                Thread.sleep(500);
            }
            Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            ChatMessage chatMessage = realm.createObject(ChatMessage.class);
            chatMessage.setMessageFrom(currentUsername);
            chatMessage.setMessage("Sent file: icon_test.png");
            chatMessage.setIsSent(true);
            chatMessage.setMessageTo(to);
            realm.commitTransaction();

            Log.d(TAG, "File transfer to " + to + " is done");
            // Now that the file transfer is done check for errors
            // or exceptions
            FileTransfer.Error error = outgoingFileTransfer.getError();
            if (error != null) {
                Log.e(TAG, error.getMessage());
            }

            Exception exception = outgoingFileTransfer.getException();

            if (exception != null) {
                if (exception instanceof XMPPException.XMPPErrorException)
                {
                    XMPPException.XMPPErrorException errorException =
                            (XMPPException.XMPPErrorException)exception;
                    XMPPError xmppError = errorException.getXMPPError();
                    Log.e(TAG, xmppError.toString());
                    Log.e(TAG, "Descriptive text: " + xmppError.getDescriptiveText());
                    Log.e(TAG, "Condition: " + xmppError.getCondition());
                    Log.e(TAG, "Type: " + xmppError.getType());
                }
            }
        }
        // For now, just catching and logging exceptions. Exception handling
        // will be added in top-level classes
        catch (SmackException e) {
            Log.e(TAG, "Exception trying to send jar file", e);
        } catch (InterruptedException e) {
            // Do nothing
        } catch (Exception e) {
            Log.e(TAG, "Exception trying to send jar file: " + e);
        }

    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        if (!createdLocally){
            chat.addMessageListener(this);
        }
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        for(int i = 0; i < message.getExtensions().size(); i++) {

            if(message.getBody() != null) {
                ChatMessage chatMessage = new ChatMessage(message);

                EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_RECEIVED, chatMessage, null));
            }
            break;
        }
    }

    public void dispatchOfflineMessages() {

        Realm realm = Realm.getInstance(context);
        RealmResults<ChatMessage> chatMessages = realm.where(ChatMessage.class)
                .equalTo(ChatMessage.MESSAGE_FROM, UserUtils.getInstance().clearUsername(connection.getUser()))
                .equalTo(ChatMessage.IS_SENT, false)
                .findAllSorted(ChatMessage.DATE_TIME);

        while(chatMessages.size() > 0) {
            Message message = new Message();
            message.setBody(chatMessages.get(0).getMessage());
            message.setFrom(chatMessages.get(0).getMessageFrom().concat("@").concat(Config.XMPP_HOST).concat("/Smack"));
            message.setTo(chatMessages.get(0).getMessageTo());

            try {
                sendMessage(message);
                Thread.sleep(50); //Giving some breasin to server
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void configureProvider() {
        ProviderManager.addIQProvider("vCard", "vcard-temp", new VCardProvider());

        ProviderManager.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());
        // Group Chat Invitations
        ProviderManager.addExtensionProvider("x", "jabber:x:conference",new GroupChatInvitation.Provider());
        // Service Discovery # Items
        ProviderManager.addIQProvider("query","http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
        // Service Discovery # Info
        ProviderManager.addIQProvider("query","http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
        // Data Forms
        ProviderManager.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
        // Delayed Delivery
        ProviderManager.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());
        // Version
        try {
            ProviderManager.addIQProvider("query", "jabber:iq:version",Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) { }
        // Offline Message Requests
        ProviderManager.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());
        // Offline Message Indicator
        ProviderManager.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());
        // Last Activity
        ProviderManager.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());
        // User Search
        ProviderManager.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());
        // SharedGroupsInfo

        ProviderManager.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());
        // JEP-33: Extended Stanza Addressing
        ProviderManager.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());


        ProviderManager.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());
        ProviderManager.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
        ProviderManager.addIQProvider("open", "http://jabber.org/protocol/ibb", new OpenIQProvider());
        ProviderManager.addIQProvider("close", "http://jabber.org/protocol/ibb", new CloseIQProvider());


        // Privacy
        ProviderManager.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
        ProviderManager.addIQProvider("command", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider());

        ProviderManager.addExtensionProvider("malformed-action","http://jabber.org/protocol/commands",new AdHocCommandDataProvider.MalformedActionError());

        ProviderManager.addExtensionProvider("bad-locale","http://jabber.org/protocol/commands",new AdHocCommandDataProvider.BadLocaleError());

        ProviderManager.addExtensionProvider("bad-payload","http://jabber.org/protocol/commands",new AdHocCommandDataProvider.BadPayloadError());

        ProviderManager.addExtensionProvider("bad-sessionid","http://jabber.org/protocol/commands",new AdHocCommandDataProvider.BadSessionIDError());

        ProviderManager.addExtensionProvider("session-expired","http://jabber.org/protocol/commands",new AdHocCommandDataProvider.SessionExpiredError());
        // Attention
        ProviderManager.addExtensionProvider("attention", "urn:xmpp:attention:0", new AttentionExtension.Provider());

        // XEP-184 Message Delivery Receipts
        ProviderManager.addExtensionProvider("received", "urn:xmpp:receipts", new DeliveryReceipt.Provider());
        ProviderManager.addExtensionProvider("request", "urn:xmpp:receipts", new DeliveryReceipt.Provider());

        ProviderManager.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
        ProviderManager.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
        ProviderManager.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
    }

    @Override
    public void connected(XMPPConnection connection) {
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        if(connection.isConnected()) {
            dispatchOfflineMessages();
        }
    }

    @Override
    public void connectionClosed() {
    }

    @Override
    public void connectionClosedOnError(Exception e) {
    }

    @Override
    public void reconnectionSuccessful() {
    }

    @Override
    public void reconnectingIn(int seconds) {
    }

    @Override
    public void reconnectionFailed(Exception e) {

    }

    @Override
    public void fileTransferRequest(FileTransferRequest request) {
        final String requestorId = request.getRequestor();
        Log.e(TAG, "FileTransferRequest from: " + requestorId);

        //TODO put here some rule to accept or not file
        if (true) {
            final IncomingFileTransfer transfer = request.accept();
            Log.e(TAG, "FileTransferRequest accepted");

            try {

                final String fileName = transfer.getFileName();
                transfer.recieveFile(new File(BASEDIR + "/Download/" + fileName));

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!transfer.isDone()) {
                            final double progress = transfer.getProgress();
                            final double progressPercent = progress * 100.0;
                            String percComplete = String.format("%1$,.2f", progressPercent);
                            Log.i(TAG, "Transfer status is: " + transfer.getStatus());
                            Log.i(TAG, "File transfer is " + percComplete + "% complete");

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        FileTransfer.Error transferError = transfer.getError();
                        if (transferError != null) {
                            Log.e(TAG, "Transfer error occurred: " + transferError.getMessage());
                        }
                    }
                }).start();

                Exception transferException = transfer.getException();
                if (transferException != null) {
                    Log.e(TAG, "Transfer exception occurred: " + transferException);
                    if (transferException instanceof SmackException.NoResponseException) {
                        SmackException.NoResponseException smackException = (SmackException.NoResponseException)transferException;
                        smackException.printStackTrace();
                    }
                }

                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setMessageFrom(requestorId);
                chatMessage.setMessage("Received file: " + fileName);
                chatMessage.setIsDisplay(true);
                chatMessage.setMessageTo(currentUsername);
                EventBus.getDefault().post(new GenericBus(GenericBus.CHAT_FILE_RECEIVED, chatMessage, null));
            } catch (SmackException e) {
                Log.e(TAG, "SmackException trying to receive jar file", e);
            } catch (IOException e) {
                Log.e(TAG, "IOException trying to receive jar file", e);
            }

        } else {
            Log.e(TAG, "FileTransferRequest rejected");
            try {
                request.reject();
            } catch (SmackException.NotConnectedException e) {
                Log.e(TAG, "NotConnectedException when rejecting FileTransferRequest");
            }
        }
    }
}
