package com.origamitecnologia.hotpotato.utils;

public class PotatoUtils {

    private static PotatoUtils instance;
    private boolean isCurrentPotatoBurned;
    private PotatoUtils() {

    }

    public static PotatoUtils getInstance() {
        if(instance == null) {
            instance = new PotatoUtils();
        }
        return instance;
    }

    public static void setInstance(PotatoUtils instance) {
        PotatoUtils.instance = instance;
    }

    public boolean isCurrentPotatoBurned() {
        return isCurrentPotatoBurned;
    }

    public void setIsCurrentPotatoBurned(boolean isCurrentPotatoBurned) {
        this.isCurrentPotatoBurned = isCurrentPotatoBurned;
    }
}
