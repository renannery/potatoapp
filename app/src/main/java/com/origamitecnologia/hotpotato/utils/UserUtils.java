package com.origamitecnologia.hotpotato.utils;

import android.content.Context;
import android.util.Patterns;

import com.origamitecnologia.hotpotato.models.User;

import java.util.Random;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmQuery;


public class UserUtils {

    private static UserUtils instance;
    private static Random rand;
    private UserUtils() {

    }

    public static UserUtils getInstance() {
        if(instance == null) {
            instance = new UserUtils();
            rand = new Random();
        }
        return instance;
    }

    public User validateUser(Context context, String username, String password) {
        Realm realm = Realm.getInstance(context);
        RealmQuery<User> query = realm.where(User.class);

        return query.equalTo("username", username).equalTo("password", password).findFirst();
    }

    public boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public boolean isUserLogged(Context context) {
        Realm realm = Realm.getInstance(context);
        RealmQuery<User> query = realm.where(User.class);
        User user = query.equalTo("mainUser", true).findFirst();
        if (user != null) {
            realm.close();
            return true;
        }
        realm.close();
        return false;
    }

    public User getLoggedUser(Context context) {
        Realm realm = Realm.getInstance(context);
        RealmQuery<User> query = realm.where(User.class);

        return query.findFirst();
    }

    public User getUserByUsername(Context context, String username) {
        Realm realm = Realm.getInstance(context);
        RealmQuery<User> query = realm.where(User.class);
        return query.equalTo("chatUsername", username).findFirst();
    }

    public void setLogoff(Context context){
        Realm realm = Realm.getInstance(context);
        RealmQuery<User> query = realm.where(User.class);
        User user = query.findFirst();
        realm.beginTransaction();
        realm.commitTransaction();
    }

    public String clearUsername(String login) {
        int signIndex = login.lastIndexOf("@");
        if (signIndex != -1) {
            return login.substring(0, signIndex);
        }
        return login;
    }

    public String randomUserPicture() {
        int randomNum = rand.nextInt((50 - 1) + 1) + 1;
        return "portraits/med/men/" + randomNum + ".jpg";
    }

}
