package com.origamitecnologia.hotpotato.utils;

public class Const {

    public static final String RECEIVED = "RECEIVED";
    public static final String POTATO_BURNED = "BURNED";

}