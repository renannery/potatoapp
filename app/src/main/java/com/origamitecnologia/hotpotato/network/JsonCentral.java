package com.origamitecnologia.hotpotato.network;

import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.origamitecnologia.hotpotato.managers.GenericBus;
import com.origamitecnologia.hotpotato.models.Potato;
import com.origamitecnologia.hotpotato.models.Result;

import de.greenrobot.event.EventBus;
import io.realm.RealmObject;

public class JsonCentral {

    private static JsonCentral instance;
    private Gson gson;

    private JsonCentral() {
        gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

    }

    public static JsonCentral getInstance() {
        if(instance == null) {
            instance = new JsonCentral();
        }
        return instance;
    }

    public void sendMessage (final JsonObject jsonUserParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().sendMessage(jsonUserParameters);
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_DELIVERY, jo.get("sentTo").getAsString(), null));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE login user: ".concat(e.toString()));
                    e.printStackTrace();
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_DELIVERY, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void updatePotatoTime (final JsonObject jsonUserParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    new APIClient().service().updatePotatoTime(jsonUserParameters);
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE login user: ".concat(e.toString()));
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void createPotato (final JsonObject jsonUserParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().createPotato(jsonUserParameters);
                    Potato potato = gson.fromJson(jo.getAsJsonObject(), Potato.class);
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_RECEIVED_CONFIRMATION, potato, null));
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_CREATED, potato, null));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE login user: ".concat(e.toString()));
                    e.printStackTrace();
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_CREATED, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void receivedPotatoConfirmation(final JsonObject jsonUserParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().receivedPotato(jsonUserParameters);
                    Potato potato = gson.fromJson(jo.getAsJsonObject(), Potato.class);
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_RECEIVED_CONFIRMATION, potato, null));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE login user: ".concat(e.toString()));
                    e.printStackTrace();
                    EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_RECEIVED_CONFIRMATION, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

}
