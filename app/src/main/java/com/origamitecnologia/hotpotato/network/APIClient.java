package com.origamitecnologia.hotpotato.network;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.origamitecnologia.hotpotato.utils.Config;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.POST;

public class APIClient {

    private static RestAdapter REST_ADAPTER;

    private static void createAdapterIfNeeded() {
        if (REST_ADAPTER == null) {
            Gson gson = new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(0, TimeUnit.MILLISECONDS);
            REST_ADAPTER = new RestAdapter.Builder()
                    .setEndpoint(Config.PREFIX)
                    .setLogLevel(LogLevel.FULL)
                    .setClient(new OkClient(client))
                    .setConverter(new GsonConverter(gson))
                    .build();
        }
    }

    public APIClient() {
        createAdapterIfNeeded();
    }

    public APIEndpointInterface service() {
        return REST_ADAPTER.create(APIEndpointInterface.class);
    }

    public interface APIEndpointInterface {

        @POST(Config.API_VERSION + "/users/sendMessage") JsonObject sendMessage(
                @Body JsonObject parameters);

        @POST(Config.API_VERSION + "/potatos/register") JsonObject createPotato(
                @Body JsonObject parameters);

        @POST(Config.API_VERSION + "/potatos/received") JsonObject receivedPotato(
                @Body JsonObject parameters);

        @POST(Config.API_VERSION + "/potatos/updateTime") JsonObject updatePotatoTime(
                @Body JsonObject parameters);
    }
}