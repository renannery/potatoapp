package com.origamitecnologia.hotpotato.models;


import com.origamitecnologia.hotpotato.utils.UserUtils;

import org.jivesoftware.smack.packet.Message;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class ChatMessage extends RealmObject {
    @Ignore
    public static final String DATE_TIME = "dateTime";
    @Ignore
    public static final String MESSAGE = "message";
    @Ignore
    public static final String MESSAGE_FROM = "messageFrom";
    @Ignore
    public static final String MESSAGE_TO = "messageTo";
    @Ignore
    public static final String MESSAGE_ID = "messageID";
    @Ignore
    public static final String IS_DISPLAY = "isDisplay";
    @Ignore
    public static final String IS_SENT = "isSent";
    @Ignore
    public static final String IS_RECEIVED = "isReceived";


    private Date dateTime;
    private String message;
    private String messageFrom;
    private String messageTo;
    private boolean isDisplay;
    private boolean isSent;
    private boolean isReceived;

    public ChatMessage() {
    }

    public ChatMessage(Message message) {
        this.message = message.getBody();
        this.messageFrom = UserUtils.getInstance().clearUsername(message.getFrom());
        this.messageTo = UserUtils.getInstance().clearUsername(message.getTo());
    }

    public boolean isSent() {
        return isSent;
    }

    public void setIsSent(boolean isSent) {
        this.isSent = isSent;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public void setIsReceived(boolean isReceived) {
        this.isReceived = isReceived;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public boolean isDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }
}
