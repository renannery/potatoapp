package com.origamitecnologia.hotpotato.models;


import com.origamitecnologia.hotpotato.utils.UserUtils;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.RosterEntry;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    private int id;
    @PrimaryKey
    private String chatUsername;
    private String fullname;

    public User () {

    }

    public User(RealmObject user) {

    }

    public User(RosterEntry rosterUser, Presence presence) {

        this.fullname = rosterUser.getName();
        this.chatUsername = UserUtils.getInstance().clearUsername(rosterUser.getUser());

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChatUsername() {
        return chatUsername;
    }

    public void setChatUsername(String chatUsername) {
        this.chatUsername = chatUsername;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
