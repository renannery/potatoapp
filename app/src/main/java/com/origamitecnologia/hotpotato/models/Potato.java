package com.origamitecnologia.hotpotato.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Potato implements Parcelable {

    public static final String POTATO = "POTATO";

    private int id;
    private int type;
    private String punishment;
    private String currentUsername;
    private String lastUsername;
    private int secondsToBurn;
    private boolean isBoom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPunishment() {
        return punishment;
    }

    public void setPunishment(String punishment) {
        this.punishment = punishment;
    }

    public String getCurrentUsername() {
        return currentUsername;
    }

    public void setCurrentUsername(String currentUsername) {
        this.currentUsername = currentUsername;
    }

    public String getLastUsername() {
        return lastUsername;
    }

    public void setLastUsername(String lastUsername) {
        this.lastUsername = lastUsername;
    }

    public int getSecondsToBurn() {
        return secondsToBurn;
    }

    public void setSecondsToBurn(int secondsToBurn) {
        this.secondsToBurn = secondsToBurn;
    }

    public boolean isBoom() {
        return isBoom;
    }

    public void setIsBoom(boolean isBoom) {
        this.isBoom = isBoom;
    }

    public Potato() {
    }

    protected Potato(Parcel in) {
        id = in.readInt();
        type = in.readInt();
        punishment = in.readString();
        currentUsername = in.readString();
        lastUsername = in.readString();
        secondsToBurn = in.readInt();
        isBoom = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeString(punishment);
        dest.writeString(currentUsername);
        dest.writeString(lastUsername);
        dest.writeInt(secondsToBurn);
        dest.writeByte((byte) (isBoom ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Potato> CREATOR = new Parcelable.Creator<Potato>() {
        @Override
        public Potato createFromParcel(Parcel in) {
            return new Potato(in);
        }

        @Override
        public Potato[] newArray(int size) {
            return new Potato[size];
        }
    };
}