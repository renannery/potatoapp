package com.origamitecnologia.hotpotato.managers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.origamitecnologia.hotpotato.fragments.FragmentLogin;
import com.origamitecnologia.hotpotato.fragments.FragmentSendPotato;
import com.origamitecnologia.hotpotato.models.User;
import com.origamitecnologia.hotpotato.utils.UserUtils;
import com.origamitecnologia.hotpotato.views.activities.MainActivity;

public class CallManager {

    public static final Intent main(Context context) {
        User user = UserUtils.getInstance().getLoggedUser(context);
        return new Intent(context, MainActivity.class);
    }

    public static final Fragment fragmentLogin() {
        Fragment fragment = new FragmentLogin();
        return fragment;
    }

    public static final Fragment fragmentSendPotato() {
        Fragment fragment = new FragmentSendPotato();
        return fragment;
    }


}
