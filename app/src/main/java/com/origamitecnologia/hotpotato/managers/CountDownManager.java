package com.origamitecnologia.hotpotato.managers;

import android.os.CountDownTimer;
import android.util.Log;

import de.greenrobot.event.EventBus;

public class CountDownManager extends CountDownTimer {

    private static CountDownManager instance;
    private int secondsRemaining;

    public CountDownManager(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public static CountDownManager getInstance(int secondsToCount) {
        if (instance == null) {
            instance = new CountDownManager(secondsToCount * 1000, 1000);
        }
        return instance;
    }

    public void startTimer() {
        instance.cancel();
        instance.start();
    }

    public void cancelTimer() {
        if(instance != null) {
            instance.cancel();
            instance = null;
        }
    }

    @Override
    public void onTick(long millisUntilFinished) {
        secondsRemaining = (int) (millisUntilFinished / 1000);
        Log.d("BoomTime", "seconds remaining: " + secondsRemaining);
    }

    @Override
    public void onFinish() {
        EventBus.getDefault().post(new GenericBus(GenericBus.POTATO_BURN, null, null));
        cancelTimer();
    }

    public int getSecondsRemaining() {
        return secondsRemaining;
    }
}