package com.origamitecnologia.hotpotato.managers;


import com.origamitecnologia.hotpotato.models.Result;

import java.util.ArrayList;

public class GenericBus<T> {

    private int key;
    private Result result = new Result();
    private ArrayList<T> objects;
    private Object object;

    public static final int POTATO_DELIVERY = 1;
    public static final int POTATO_RECEIVED = 2;
    public static final int POTATO_CREATED = 3;
    public static final int POTATO_RECEIVED_CONFIRMATION = 4;
    public static final int POTATO_BURN = 5;

    /* CHAT */
    public static final int CHAT_MESSAGE_COMPOSING = 12;
    public static final int CHAT_MESSAGE_PAUSED = 13;
    public static final int CHAT_MESSAGE_RECEIVED = 14;
    public static final int CHAT_MESSAGE_ACTIVE = 15;
    public static final int CHAT_MESSAGE_DISPLAYED = 16;
    public static final int CHAT_MESSAGE_BODY = 17;
    public static final int CHAT_MESSAGE_GONE = 18;
    public static final int CHAT_FILE_RECEIVED = 19;

    /* Connection State */

    public static final int CONNECTION_STATE = 20;


    public GenericBus(int key) {
        this.key = key;
        this.object = null;
    }

    public GenericBus(int key, ArrayList<T> objects, Result result) {
        this.key = key;
        this.objects = objects;
        this.result = result;
    }

    public GenericBus(int key, Object object, Result result) {
        this.key = key;
        this.object = object;
        this.result = result;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public ArrayList<T> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<T> objects) {
        this.objects = objects;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}