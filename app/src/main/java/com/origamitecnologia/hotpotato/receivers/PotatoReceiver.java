package com.origamitecnologia.hotpotato.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.origamitecnologia.hotpotato.services.PotatoService;

public class PotatoReceiver extends BroadcastReceiver {
    public PotatoReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent service = new Intent(context, PotatoService.class);
            context.startService(service);
        }
    }
}
