package com.origamitecnologia.hotpotato.views.activities;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;

import com.origamitecnologia.hotpotato.R;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public abstract class BaseActivity extends AppCompatActivity {

    private static String DRAWER_NULL_EXCEPTION = "There's no drawer associated with this activity.";
    private static String TOOLBAR_NULL_EXCEPTION = "There's no toolbar associated with this activity.";


    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutToInflate());
        ButterKnife.inject(this);
        fragmentManager = getSupportFragmentManager();
        setToolbar();
        setDrawer();

        if(savedInstanceState == null) {
            doOnFirstTime();
        }

        doOnCreated(savedInstanceState);
    }

    protected abstract void doOnFirstTime();
    protected abstract void doOnCreated(Bundle savedInstanceState);

    protected abstract int layoutToInflate();

    protected abstract boolean hasDrawer();

    private void setDrawer() {
        if(hasDrawer()) {
//            drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//            if(drawerLayout == null) {
//                throw new NullPointerException(DRAWER_NULL_EXCEPTION);
//            }
        }
    }

    public void setDrawerListener(ActionBarDrawerToggle drawerToggle) {
        drawerLayout.setDrawerListener(drawerToggle);
    }

    public DrawerLayout getDrawer() {
        return drawerLayout;
    }

    protected boolean isDrawerOpen() {
        return ((hasDrawer()) && (drawerLayout.isDrawerOpen(Gravity.LEFT)));
    }

    protected boolean isDrawerClosed() {
        return ((hasDrawer()) && (!drawerLayout.isDrawerOpen(Gravity.LEFT)));
    }

    protected boolean openDrawer() {
        if(isDrawerClosed()) {
            drawerLayout.openDrawer(Gravity.LEFT);
            return true;
        }
        return false;
    }

    protected boolean closeDrawer() {
        if(isDrawerOpen()) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return true;
        }
        return false;
    }

    public void lockDrawer() {
        if(hasDrawer()) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void unlockDrawer() {
        if(hasDrawer()) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                if(openDrawer()) {
                    return true;
                }
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(!closeDrawer()) {
            super.onBackPressed();
        }
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        return findViewById(android.R.id.content).getHeight();
    }

    public void setToolbar() {
        setToolbar((Toolbar) findViewById(R.id.toolbar));
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        if(this.toolbar != null) {
            setSupportActionBar(this.toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    public Toolbar getToolbar() {
        if(toolbar == null) {
            throw new NullPointerException(TOOLBAR_NULL_EXCEPTION);
        }
        return toolbar;
    }

    protected void registerOnEventBus() {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    protected void unregisterOnEventBus() {
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterOnEventBus();
        super.onDestroy();
    }


    public void setFragmentMain(Fragment fragment, boolean backStack) {
        String fragmentName = fragment.getClass().getName();
        FragmentTransaction fragmentTransaction;

        if(fragmentManager.findFragmentByTag(fragmentName) != null) {
            Log.d("FRAGMENT", "fragment already added");
            if(fragmentManager.findFragmentByTag(fragmentName).isVisible()) {
                return;
            } else {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(fragmentManager.findFragmentByTag(fragmentName)).commit();
            }
        } else {
            Log.d("FRAGMENT", "fragment is not added");
        }

        fragmentTransaction = fragmentManager.beginTransaction();
        if(backStack) {
            fragmentTransaction
                    .addToBackStack(fragmentName);
        }

        fragmentTransaction
                .replace(R.id.flMainContent, fragment, fragmentName)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    protected void resetToHomeFragment() {
        for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }
}
