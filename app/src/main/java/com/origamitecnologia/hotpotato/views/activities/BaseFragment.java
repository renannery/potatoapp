package com.origamitecnologia.hotpotato.views.activities;

import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.origamitecnologia.hotpotato.R;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {
    private static String TOOLBAR_NULL_EXCEPTION = "There's no toolbar associated with this fragment.";

    private Toolbar toolbar;
    private Bundle savedInstanceState;
    View rootView;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(layoutToInflate(), container, false);
            ButterKnife.inject(this, rootView);
            doOnCreated(rootView);
            setUpToolbar(rootView);
        } else {
//            ((ViewGroup) rootView.getParent()).removeView(rootView);
            setUpToolbar(rootView);
        }
        return rootView;
    }

    protected abstract int layoutToInflate();
    protected abstract void doOnCreated(View view);
    protected abstract void setUpToolbar(View view);

    protected abstract boolean isDrawerLocked();

//    private void manageDrawer() {
//        if(isDrawerLocked()) {
//            getBaseActivity().lockDrawer();
//        } else {
//            getBaseActivity().unlockDrawer();
//        }
//    }


    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    protected void doOnStateRestored(Bundle savedInstanceState) { }
    protected void doOnWithoutRestore() { }

    public Toolbar getToolbar() {
        if(toolbar == null) {
            throw new NullPointerException(TOOLBAR_NULL_EXCEPTION);
        }
        return toolbar;
    }

    protected int getActionBarSize() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        return activity.findViewById(android.R.id.content).getHeight();
    }

    protected int getScreenWidth() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        return activity.findViewById(android.R.id.content).getWidth();
    }
}
