package com.origamitecnologia.hotpotato.views.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.origamitecnologia.hotpotato.R;

import java.io.IOException;

import butterknife.InjectView;
import pl.droidsonroids.gif.GifImageView;


public class AnimationActivity extends BaseActivity implements View.OnTouchListener {
    @InjectView(R.id.gifBanana)
    GifImageView gifBanana;

    ViewGroup root;
    private int xDelta;
    private int yDelta;
    AnimatorSet scaleDown;
    AnimatorSet scaleUp;
    MediaPlayer mp;
    @Override
    protected int layoutToInflate() {
        return R.layout.activity_animation;
    }

    @Override
    protected void doOnFirstTime() { }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        root = (ViewGroup) findViewById(R.id.root);
        gifBanana.setOnTouchListener(this);

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(gifBanana, View.SCALE_X, 0.5f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(gifBanana, View.SCALE_Y, 0.5f);

        ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(gifBanana, View.SCALE_X, 1f);
        ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(gifBanana, View.SCALE_Y, 1f);

        scaleDownX.setDuration(200);
        scaleDownY.setDuration(200);

        scaleUpX.setDuration(200);
        scaleUpY.setDuration(200);

        scaleDown = new AnimatorSet();
        scaleUp = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleUp.play(scaleUpX).with(scaleUpY);
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int rawX = (int) event.getRawX();
        final int rawY = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                xDelta = rawX - lParams.leftMargin;
                yDelta = rawY - lParams.topMargin;
                scaleDown.start();
                scaleUp.cancel();
                mp = MediaPlayer.create(this, R.raw.ouch);
                mp.start();
                break;
            case MotionEvent.ACTION_UP:
                scaleDown.cancel();
                scaleUp.start();
                if(mp.isPlaying())
                    mp.stop();
                mp.reset();
                mp.release();
                mp = null;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = rawX - xDelta;
                layoutParams.topMargin = rawY - yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        root.invalidate();
        return true;
    }
}
