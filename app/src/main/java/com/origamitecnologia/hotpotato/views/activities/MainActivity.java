package com.origamitecnologia.hotpotato.views.activities;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.origamitecnologia.hotpotato.R;
import com.origamitecnologia.hotpotato.managers.CallManager;
import com.origamitecnologia.hotpotato.models.Potato;
import com.origamitecnologia.hotpotato.utils.Const;
import com.origamitecnologia.hotpotato.utils.UserUtils;


public class MainActivity extends BaseActivity {
    public static boolean isActive = false;

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_main;
    }

    @Override
    protected void doOnFirstTime() {
        isActive = true;
        if(UserUtils.getInstance().getLoggedUser(this) != null) {
            Intent myIntent = getIntent();
            boolean receivedPotato = myIntent.getBooleanExtra(Const.RECEIVED, false);
            boolean potatoBurned = myIntent.getBooleanExtra(Const.POTATO_BURNED, false);
            Potato potato = myIntent.getParcelableExtra(Potato.POTATO);
            Bundle bundle = new Bundle();
            bundle.putBoolean(Const.RECEIVED, receivedPotato);
            bundle.putBoolean(Const.POTATO_BURNED, potatoBurned);
            bundle.putParcelable(Potato.POTATO, potato);

            Fragment fragment = CallManager.fragmentSendPotato();
            fragment.setArguments(bundle);
            setFragmentMain(fragment, false);
        } else {
            setFragmentMain(CallManager.fragmentLogin(), false);
        }
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActive = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isActive = true;
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }
}
