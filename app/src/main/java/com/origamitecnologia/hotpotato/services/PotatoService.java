package com.origamitecnologia.hotpotato.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.origamitecnologia.hotpotato.R;
import com.origamitecnologia.hotpotato.interfaces.ILocationTracker;
import com.origamitecnologia.hotpotato.managers.CountDownManager;
import com.origamitecnologia.hotpotato.managers.GenericBus;
import com.origamitecnologia.hotpotato.managers.PositionManager;
import com.origamitecnologia.hotpotato.models.ChatMessage;
import com.origamitecnologia.hotpotato.models.Potato;
import com.origamitecnologia.hotpotato.models.User;
import com.origamitecnologia.hotpotato.network.JsonCentral;
import com.origamitecnologia.hotpotato.utils.Const;
import com.origamitecnologia.hotpotato.utils.PotatoUtils;
import com.origamitecnologia.hotpotato.utils.UserUtils;
import com.origamitecnologia.hotpotato.utils.XMPPUtils;
import com.origamitecnologia.hotpotato.views.activities.MainActivity;

import de.greenrobot.event.EventBus;

public class PotatoService extends Service implements ILocationTracker {

    private NotificationManager notificationManager;
    Uri alarmSound;
    private Location mLastLocation;
    private boolean potatoBurned = false;

    @Override
    public void onCreate() {
        super.onCreate();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if(PositionManager.getInstance() == null) {
            PositionManager.getInstance().initialize(this, this);
        }
        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        XMPPUtils.getInstance(this).connect();
        XMPPUtils.getInstance(this).login();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Service started");

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        Potato potato;

        System.out.println("Service Running");
        if (event.getKey() == GenericBus.POTATO_BURN && !MainActivity.isActive) {
            Toast.makeText(this, "Boooomm!!!", Toast.LENGTH_SHORT).show();
            PotatoUtils.getInstance().setIsCurrentPotatoBurned(true);
        }

        if (event.getKey() == GenericBus.POTATO_RECEIVED_CONFIRMATION && !MainActivity.isActive) {
            potato = (Potato) event.getObject();
            CountDownManager.getInstance(potato.getSecondsToBurn()).start();

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.common_signin_btn_icon_dark)
                            .setContentTitle("Batata")
                            .setSound(alarmSound)
                            .setAutoCancel(true)
                            .setContentText(potato.getLastUsername() + " Te mandou uma batata Quente, Quente, Quente!");

            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra(Const.RECEIVED, true);
            resultIntent.putExtra(Potato.POTATO, potato);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

            stackBuilder.addParentStack(MainActivity.class);

            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            notificationManager.notify(001, mBuilder.build());
        }

        if (event.getKey() == GenericBus.POTATO_RECEIVED ) {

            if(PositionManager.getInstance() == null) {
                PositionManager.getInstance().initialize(this, this);
            } else {
                mLastLocation = PositionManager.getInstance().getLocation(this);
                if (mLastLocation == null) {
                    Location location = new Location("");
                    location.setLatitude(0);
                    location.setLongitude(0);
                    mLastLocation = location;
                }
            }
            ChatMessage chatMessage = (ChatMessage) event.getObject();

            if(!MainActivity.isActive) {
                User user = UserUtils.getInstance().getLoggedUser(this);
                JsonObject jsonParameters = new JsonObject();
                jsonParameters.addProperty("username", user.getChatUsername().toLowerCase());
                jsonParameters.addProperty("latitude", mLastLocation.getLatitude());
                jsonParameters.addProperty("longitude", mLastLocation.getLongitude());
                jsonParameters.addProperty("idPotato", Integer.parseInt(chatMessage.getMessage()));
                jsonParameters.addProperty("lastUsername", chatMessage.getMessageFrom());
                JsonCentral.getInstance().receivedPotatoConfirmation(jsonParameters);


            }
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected() {
        mLastLocation = PositionManager.getInstance().getLocation(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = PositionManager.getInstance().getLocation(this);
        if(mLastLocation == null) {
            mLastLocation = location;
        }
    }


}