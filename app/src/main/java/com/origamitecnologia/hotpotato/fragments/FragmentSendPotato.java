package com.origamitecnologia.hotpotato.fragments;

import android.content.Intent;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.origamitecnologia.hotpotato.R;
import com.origamitecnologia.hotpotato.interfaces.ILocationTracker;
import com.origamitecnologia.hotpotato.managers.CountDownManager;
import com.origamitecnologia.hotpotato.managers.GenericBus;
import com.origamitecnologia.hotpotato.managers.PositionManager;
import com.origamitecnologia.hotpotato.models.ChatMessage;
import com.origamitecnologia.hotpotato.models.Potato;
import com.origamitecnologia.hotpotato.models.User;
import com.origamitecnologia.hotpotato.network.JsonCentral;
import com.origamitecnologia.hotpotato.services.PotatoService;
import com.origamitecnologia.hotpotato.utils.Const;
import com.origamitecnologia.hotpotato.utils.PotatoUtils;
import com.origamitecnologia.hotpotato.utils.UserUtils;
import com.origamitecnologia.hotpotato.views.activities.AnimationActivity;
import com.origamitecnologia.hotpotato.views.activities.BaseFragment;

import java.util.Random;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class FragmentSendPotato extends BaseFragment implements ILocationTracker {

    @InjectView(R.id.btSendPotato)
    Button btSendPotato;

    @InjectView(R.id.btCreatePotato)
    Button btCreatePotato;

    @InjectView(R.id.btAnimate)
    Button btAnimate;

    @InjectView(R.id.etPunishment)
    EditText etPunishment;

    @InjectView(R.id.tvLastUser)
    TextView tvLastUser;

    @InjectView(R.id.tvCreatedPotato)
    TextView tvCreatedPotato;

    @InjectView(R.id.tvNextUser)
    TextView tvNextUser;

    @InjectView(R.id.tvLatLong)
    TextView tvLatLong;

    @InjectView(R.id.viewLeft)
    View viewLeft;

    @InjectView(R.id.viewRight)
    View viewRight;

    private Potato potato;
    private Location currentLocation;

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btSendPotato)
    public void onSendPotatoClick() {
        User user = UserUtils.getInstance().getLoggedUser(getActivity());

        JsonObject jsonUserParameters = new JsonObject();
        jsonUserParameters.addProperty("fromIdUser", user.getChatUsername().toLowerCase());
        jsonUserParameters.addProperty("idPotato", potato.getId());
        JsonCentral.getInstance().sendMessage(jsonUserParameters);

        JsonObject jsonPotatoParameters = new JsonObject();
        jsonPotatoParameters.addProperty("idPotato", potato.getId());
        jsonPotatoParameters.addProperty("secondsToBurn", CountDownManager.getInstance(0).getSecondsRemaining());
        JsonCentral.getInstance().updatePotatoTime(jsonPotatoParameters);
        CountDownManager.getInstance(0).cancelTimer();
        btSendPotato.setEnabled(false);
    }

    @OnClick(R.id.btAnimate)
    public void onBtAnimateClick() {
        startActivity(new Intent(getActivity(), AnimationActivity.class));
    }

    @OnClick(R.id.btCreatePotato)
    public void onCreatePotatoClick() {
        User user = UserUtils.getInstance().getLoggedUser(getActivity());

        Random rand = new Random();

        int secondsToBurn = rand.nextInt((60 - 1) + 1) + 1;

        JsonObject jsonUserParameters = new JsonObject();
        jsonUserParameters.addProperty("punishment", etPunishment.getText().toString());
        jsonUserParameters.addProperty("createdByUser", user.getChatUsername());
        jsonUserParameters.addProperty("secondsToBurn", secondsToBurn);
        JsonCentral.getInstance().createPotato(jsonUserParameters);
        CountDownManager.getInstance(0).cancelTimer();
        btSendPotato.setEnabled(true);
    }

    public void onEventMainThread(GenericBus event) {

        if (event.getKey() == GenericBus.POTATO_RECEIVED) {
            User user = UserUtils.getInstance().getLoggedUser(getActivity());

            ChatMessage chatMessage = (ChatMessage) event.getObject();
            tvLastUser.setText(chatMessage.getMessageFrom() + " te passou a batata " + chatMessage.getMessage());
            tvNextUser.setText("");
            btSendPotato.setEnabled(true);
            potato.setId(Integer.parseInt(chatMessage.getMessage()));

            if (currentLocation == null) {
                Location location = new Location("");
                location.setLatitude(0);
                location.setLongitude(0);
                currentLocation = location;
            }

            JsonObject jsonParameters = new JsonObject();
            jsonParameters.addProperty("username", user.getChatUsername().toLowerCase());
            jsonParameters.addProperty("latitude", currentLocation.getLatitude());
            jsonParameters.addProperty("longitude", currentLocation.getLongitude());
            jsonParameters.addProperty("idPotato", potato.getId());
            jsonParameters.addProperty("lastUsername", chatMessage.getMessageFrom());
            JsonCentral.getInstance().receivedPotatoConfirmation(jsonParameters);
        }

        if (event.getKey() == GenericBus.POTATO_RECEIVED_CONFIRMATION) {
            Potato potato = (Potato) event.getObject();
            CountDownManager.getInstance(potato.getSecondsToBurn()).start();
        }

        if (event.getKey() == GenericBus.POTATO_BURN) {
            Toast.makeText(getActivity(), "Boooomm!!!", Toast.LENGTH_SHORT).show();
            btSendPotato.setEnabled(false);

            MediaPlayer mPlayer = MediaPlayer.create(getActivity(), R.raw.mgun);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.start();
        }

        if (event.getKey() == GenericBus.POTATO_DELIVERY) {
            String sentTo = (String) event.getObject();
            tvLastUser.setText("");
            tvNextUser.setText("Batata enviada para -> " + sentTo);
            CountDownManager.getInstance(0).cancelTimer();
        }

        if (event.getKey() == GenericBus.POTATO_CREATED) {
            potato = (Potato) event.getObject();
            tvCreatedPotato.setText("Batata criada id: " + potato.getId() + "\n" + "Punicao:" + potato.getPunishment() + "\n" + "Boom:" + potato.getSecondsToBurn());

        }

    }


    @Override
    public int layoutToInflate() {
        return R.layout.fragment_send_potato;
    }

    @Override
    public void doOnCreated(View view) {
        potato = new Potato();
        PositionManager.getInstance().initialize(this, getActivity());

        if(getArguments() != null) {
            if (getArguments().getBoolean(Const.RECEIVED)) {
                potato = getArguments().getParcelable(Potato.POTATO);
                tvLastUser.setText(potato.getLastUsername() + " te passou a batata " + potato.getId());
                tvNextUser.setText("");
                btSendPotato.setEnabled(!PotatoUtils.getInstance().isCurrentPotatoBurned());
            }
        }

        User user = UserUtils.getInstance().getLoggedUser(getActivity());
        if (user.getChatUsername().equals("renannery10")) {
            btSendPotato.setEnabled(true);
            btCreatePotato.setEnabled(true);
        }

        Intent intent = new Intent(getActivity(), PotatoService.class);
        getActivity().startService(intent);

    }

    public void animateFeets() {
//        ObjectAnimator viewLeftAnimator = ObjectAnimator.ofFloat(viewLeft, View.ROTATION_X, 0, 360);
//        ObjectAnimator viewRightAnimator = ObjectAnimator.ofFloat(viewRight, View.TRANSLATION_X, 0, 100);
//
//        viewLeftAnimator.setInterpolator(new DecelerateInterpolator(1.8f));
//        viewRightAnimator.setInterpolator(new DecelerateInterpolator(1.8f));

        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.pendulum);
        viewRight.startAnimation(animation);

    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return true;
    }

    @Override
    public void onConnected() {
        currentLocation = PositionManager.getInstance().getLocation(getActivity());
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = PositionManager.getInstance().getLocation(getActivity());
        if(currentLocation == null) {
            currentLocation = location;
        }
    }
}
