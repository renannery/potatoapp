package com.origamitecnologia.hotpotato.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.origamitecnologia.hotpotato.R;
import com.origamitecnologia.hotpotato.managers.CallManager;
import com.origamitecnologia.hotpotato.models.User;
import com.origamitecnologia.hotpotato.views.activities.BaseFragment;
import com.origamitecnologia.hotpotato.views.activities.MainActivity;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;

public class FragmentLogin extends BaseFragment {

    @InjectView(R.id.etLogin)
    EditText etLogin;

    @InjectView(R.id.etPassword)
    EditText etPassword;

    @InjectView(R.id.btLogin)
    Button btLogin;

    @OnClick(R.id.btLogin)
    public void onLoginClick() {

        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        User user = realm.createObject(User.class);
        user.setChatUsername(etLogin.getText().toString());
        realm.commitTransaction();
        realm.close();

        goToSendPotato();

    }

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_login;
    }

    @Override
    public void doOnCreated(View view) {

    }

    @Override
    protected void setUpToolbar(View view) {

    }

    private void goToSendPotato() {
        ((MainActivity) getActivity()).setFragmentMain(CallManager.fragmentSendPotato(), false);
    }

    @Override
    protected boolean isDrawerLocked() {
        return true;
    }
}
